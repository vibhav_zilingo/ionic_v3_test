* node 7.10.1
* npm 5.6.0
* angular 5

* ionic 3.8.1
* cordova 7.1.0
* cordova-android 8.1.0
* config.xml "android-minSdkVersion" value="19"

### ionic info

cli packages: (/home/zilingo/.nvm/versions/node/v7.10.1/lib/node_modules)

    @ionic/cli-utils  : 1.8.1
    ionic (Ionic CLI) : 3.8.1

global packages:

    Cordova CLI : 7.1.0 

local packages:

    @ionic/app-scripts : 3.1.0
    Cordova Platforms  : android 8.1.0
    Ionic Framework    : ionic-angular 3.9.2

System:

    Android SDK Tools : 26.1.1
    Node              : v7.10.1
    npm               : 5.6.0 
    OS                : Linux 5.3
