import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Pro, AppInfo, DeployInfo, ProDeploy, ProgressMessage } from '@ionic-native/pro';

/**
 * Generated class for the UpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update',
  templateUrl: 'update.html',
})
export class UpdatePage {

  deploy: ProDeploy;

  dataAppInfo: AppInfo;
  
  dataCheck: string;
  dataDownload: ProgressMessage;
  dataExtract: ProgressMessage;
  dataInfo: DeployInfo;
  dataVersions: string[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private pro: Pro, public toastCtrl: ToastController) {
    this.deploy = this.pro.deploy();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdatePage');
  }

  getAppInfo() {
    this.pro.getAppInfo()
      .then((res: AppInfo) => {
        this.dataAppInfo = res;
      })
      .catch(err => {
        this.showToast(`AppInfo Error: ${JSON.stringify(err)}`, true);
        console.error(err)
      });
  }

  deployInfo() {
    this.deploy.info()
      .then((res) => {
        this.dataInfo = res;
      })
      .catch(err => {
        this.showToast(`Info Error: ${JSON.stringify(err)}`, true);
        console.error(err)
      });
  }

  deployCheck() {
    this.deploy.check()
      .then((res) => {
        this.dataCheck = res;
      })
      .catch(err => {
        this.showToast(`Check Error: ${JSON.stringify(err)}`, true);
        console.error(err)
      });
  }

  deployDownload() {
    this.deploy.download()
      .subscribe(
        dlres => {
          console.log(">>> Download progress: " + dlres);
          this.dataDownload = dlres;
        },
        err => {
          this.showToast(`Download Error: ${JSON.stringify(err)}`, true);
          console.error(err);
        },
        () => {
          this.showToast('Download complete', false);
        }
      );
  }
  
  deployExtract() {
    this.deploy.extract()
      .subscribe(
        extres => {
          console.log(">>> Extract progress: " + extres);
          this.dataExtract = extres;
        },
        err => {
          this.showToast(`Extract Error: ${JSON.stringify(err)}`, true);
          console.error(err);
        },
        () => {
          this.showToast('Extract complete', false);
        }
      );
  }
  
  deployRedirect() {
    this.deploy.redirect()
      .then((res) => {
        this.showToast('Deploy Redirect', false);
      })
      .catch(err => {
        this.showToast(`Redirect Error: ${JSON.stringify(err)}`, true);
        console.error(err)
      });
  }
    
  deployGetVersions() {
    this.deploy.getVersions()
      .then((res) => {
        this.dataVersions = res;
        this.showToast('Get Versions Complete', false);
      })
      .catch(err => {
        this.showToast(`Get Version Error: ${JSON.stringify(err)}`, true);
        console.error(err)
      });
  }
  
  deployDeleteVersion(version: string) {
    this.deploy.deleteVersion(version)
      .then((res) => {
        this.showToast(`Delete Version: ${version}`, false);
        this.deployGetVersions();
      })
      .catch(err => {
        this.showToast(`Check Error: ${JSON.stringify(err)}`, true);
        console.error(err)
      });
  }

  showToast(message: string, closeButton: boolean = false) {
    const toast = this.toastCtrl.create({
      message,
      showCloseButton: closeButton,
      closeButtonText: 'Ok',
      duration: 2000,
    });
    toast.present();
  }

}

// IonicCordova.deploy.init(config, success, failure) - Initializes the plugin with an app ID and API host specified in js-land. Can be used to change these variables at runtime.
// IonicCordova.deploy.check(success, failure) - Check for updates from a specified channel, will change the saved channel from the install step.
// IonicCordova.deploy.download(success, failure) - If an update is present, download it.
// IonicCordova.deploy.extract(success, failure) - If an update has been downloaded, extract it and set the default redirect location for next app start.
// IonicCordova.deploy.redirect(success, failure) - Redirect to the latest version of the app on this device.
// IonicCordova.deploy.info(success, failure) - Get info on current version for this device.
// IonicCordova.deploy.getVersions(success, failure) - List downloaded versions on this device.
// IonicCordova.deploy.deleteVersion(uuid, success, failure) - Delete a downloaded version by UUID from this device.

      // Get app info
      // this.pro.getAppInfo().then((res: AppInfo) => {
      //   console.log(res)
      // }).catch(err => {console.error(err)})
      
      // const deploy = this.pro.deploy();

      // deploy.check().then(checkResponse => {
      //   console.log(checkResponse);
      //   if(checkResponse === 'true') {
      //     deploy.download().subscribe(dlResponse => {
      //       console.log(">>> Download progress: " + dlResponse);
      //       if(dlResponse === 'true' || dlResponse == 'false') {
      //         deploy.extract().subscribe(extractResponse => {
      //           console.log(extractResponse);
      //           if(extractResponse === 'true' || extractResponse == 'false' || extractResponse == 'done') {
      //             console.log(">>> Redirect");
      //             deploy.redirect();
      //           }else{
      //             // It's a progress update
      //             console.log('>>> Extract progress:', extractResponse);
      //           }
      //         });
      //       }
      //     })
      //   } else {
      //     // It's a progress update
      //     console.log('>>> Ionic update not needed');
      //   }
      // });


      // check(): Promise<string>;
      // download(): Observable<ProgressMessage>;
      // extract(): Observable<ProgressMessage>;
      // redirect(): Promise<void>;
      // info(): Promise<DeployInfo>;
      // getVersions(): Promise<string[]>;
      // deleteVersion(version: string): Promise<void>;
  

      // deploy.check().then(res => {
      //   console.log(res)
      // }).catch(err => {console.error(err)});

      // deploy.info().then(res => {
      //   console.log(res)
      // }).catch(err => {console.error(err)});
